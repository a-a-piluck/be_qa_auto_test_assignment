package starter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DataVerificationUtils {

    public static Set<String> oneOfSubstringIsPresentInListOfStrings(String[] substrings, ArrayList<String> strings){
        Set<String> missMatchedItems = new HashSet<>();
        for (String string : strings) {
            for (String substring : substrings) {
                if (string.toLowerCase().contains(substring.toLowerCase())) {
                    missMatchedItems.remove(string);
                    break;
                } else { missMatchedItems.add(string); }
            }
        }
        return missMatchedItems;
    }
}
