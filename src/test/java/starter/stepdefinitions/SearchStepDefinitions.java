package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import starter.DataVerificationUtils;

import java.util.ArrayList;
import java.util.Set;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

public class SearchStepDefinitions {

    @When("the user calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("the user sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.contentType(ContentType.JSON));
        String[] apples = {"apple", "appel", "appelsap", "jumbo sap pakket","granny smith", "pink lady pinkids",
                                "royal gala", "lady cripps pink", "fuji"};
        Response response = SerenityRest.lastResponse();
        ArrayList<String> productTitles = response.body().path("title");
        Set<String> missMatchedItems = DataVerificationUtils.oneOfSubstringIsPresentInListOfStrings(apples, productTitles);
        assertEquals(String.format("Expected 'apple products' to be present in the next string(s): %s", missMatchedItems), 0, missMatchedItems.size());
    }

    @Then("the user sees the results displayed for mango")
    public void heSeesTheResultsDisplayedForMango() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.contentType(ContentType.JSON));
        String[] mango = { "mango" };
        Response response = SerenityRest.lastResponse();
        ArrayList<String> productTitles = response.body().path("title");
        Set<String> missMatchedItems = DataVerificationUtils.oneOfSubstringIsPresentInListOfStrings(mango, productTitles);
        assertEquals(String.format("Expected 'mango products' to be present in the next string(s): %s", missMatchedItems), 0, missMatchedItems.size());
    }

    @Then("the user sees the results displayed for tofu")
    public void heSeesTheResultsDisplayedForTofu() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.contentType(ContentType.JSON));
        String[] tofu = { "tofu" };
        Response response = SerenityRest.lastResponse();
        ArrayList<String> productTitles = response.body().path("title");
        Set<String> missMatchedItems = DataVerificationUtils.oneOfSubstringIsPresentInListOfStrings(tofu, productTitles);
        assertEquals(String.format("Expected 'tofu products' to be present in the next string(s): %s", missMatchedItems), 0, missMatchedItems.size());
    }

    @Then("the user sees the results displayed for water")
    public void heSeesTheResultsDisplayedForWater() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.contentType(ContentType.JSON));
        String[] water = { "water" , "spa touch", "crystal clear", "spa reine", "spa intense", "clear granaatappe",
                            "jumbo helder bruisend", "sanpellegrino limonata", "spa finesse fles", "s.pellegrino",
                            "woon clear citroen", "woon tonic classic"};

        Response response = SerenityRest.lastResponse();
        ArrayList<String> productTitles = response.body().path("title");
        Set<String> missMatchedItems = DataVerificationUtils.oneOfSubstringIsPresentInListOfStrings(water, productTitles);
        assertEquals(String.format("Expected 'water products' to be present in the next string(s): %s", missMatchedItems), 0, missMatchedItems.size());
    }

    @Then("the user does not see the results")
    public void he_Does_Not_See_The_Results() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.contentType(ContentType.JSON));
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

}
