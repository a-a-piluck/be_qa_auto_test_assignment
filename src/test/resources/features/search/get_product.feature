Feature: Search for the product

  Scenario: User can find products containing the apple
    When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple"
    Then the user sees the results displayed for apple

  Scenario: User can find products containing the mango
    When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/mango"
    Then the user sees the results displayed for mango

  Scenario: User can find products containing the tofu
    When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/tofu"
    Then the user sees the results displayed for tofu

  Scenario: User can find products containing the water
    When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/water"
    Then the user sees the results displayed for water

  Scenario: User gets proper error message for product that doesn't exist
    When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/car"
    Then the user does not see the results
