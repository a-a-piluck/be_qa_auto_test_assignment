# BE_QA_AUTO_TEST_ASSIGNMENT

## Getting started

#### Prerequisites
- Install JDK 8.
- To run tests from the commandline - install Maven3.
- To run tests from the IDE - install IntellijIdea CE, Cucumber for Java plugin, Maven plugin.

#### How to run tests from the command line
- Make sure you have followed instructions from the "Prerequisites -> To run tests from the commandline" section.
- Open terminal.
- Execute ```mvn clean verify```.

#### How to run tests from the IDE
- Make sure you have followed instructions from the "Prerequisites -> To run tests from the IDE" section.
- Find a feature file at "PROJECT_ROOT/src/test/resources/features/"
- Open the feature file
- Find a green triangle(s) and click on it
- Select option "Run"

#### How to see the test execution report
- Run Tests.
- Once tests will finish, open the project -> find the 'target' folder -> site -> serenity -> open in the browser "index.html".

#### How to write new tests
- Open "PROJECT_ROOT/src/test/resources/features/".
- Naming convention: Use "_" (underscore) instead of " " (space) symbol, avoid any special characters like $#;№ etc.
- Create a new folder "feature_name".  For example, "Search".
- Create a new file "functionality.feature". For example, "get_cars.feature".
- Add section "Feature: The name of the feature". For example, "Feature: Search for the product".
- Add section "Scenario: The case that you want to test". For example, "Scenario: User can find products containing the apple".
- Add "When" for conditions. For example, "When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple""
- Add "Then" for assumptions. For example, "Then the user sees the results displayed for apple". 
- Each step definition must have at least 3 asserts. 1st is for the status code, 2nd is for the validation of data that was returned, 
  3rd assert for the content type.
- Add step definitions to the "PROJECT_ROOT/src/test/java/starter/stepdefinitions/FeatureNameStepDefinitions.java". 
  For example:
```
  @When("the user calls endpoint {string}")
  public void heCallsEndpoint(String arg0) {
  SerenityRest.given().get(arg0);
  }
```
- {string} - is a placeholder for variable "String arg0". String arg0 is a container for value passed in "the user calls endpoint {string}". 
For example
```  When the user calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/test/apple" ```
The value surrounded with "" is passed to variable arg0 and then can be read in the code of class for step definitions FeatureNameStepDefinitions.java

#### How to see test report in CI/CD
- Open the project in GitLab
- Find "CI/CD" menu item
- Click on the latest job/pipeline. For example "#470617073"
- Click on "test" stage
- Press "Ctrl (or CMD) + F" and type "Job artifacts"
- Click "Browse". Find folder "target" -> "site" -> "serenity" -> Open file "index.html"
  ![Alt text](tutorial/CI_CD_Pipeline.png?raw=true "CI/CD")
  ![Alt text](tutorial/JOB_Artifacts.png?raw=true "Job Artifacts / Test report")


#### What was refactored
	1) Splitted scenarios
	2) Renamed feature file "post_product.feature" to "get_product.feature" because we are testing that we can GET products.
	3) Refactored heSeesTheResultsDisplayedForMango(). Created util method for data validation and moved it to "src/main/java/starter/DataVerificationUtils.java".
	4) Deleted CarsAPI because it wasn't used. Followed YAGNI principle.
	5) According to the Gherkin specification replaced "he" with "the user".
	6) Removed Gradle, MacOS folders
    7) Added .gitignore file to avoid mess and sensitive data leak.
    8) Updated the project name in serenity.properties